var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

function introduce(name, age, address, hobby) {
    console.log('Nama Saya ' + name + ', umur saya ' + age + ' tahun, ' +
    'alamat saya di ' + address + ' dan saya punya hobby yaitu ' + hobby +
    '!')
}

introduce(name, age, address, hobby)