class Animal {
    constructor(name, legs, cold_blooded) {
        this._name = name;
        legs = this._legs = 4;
        cold_blooded = this._cold_blooded = false;
    }
    get name() {
        return this._name
    }
    set name(value) {
        this._name = value
    }
    get legs() {
        return this._legs
    }
    set legs(value) {
        this._legs = value
    }
    get cold_blooded() {
        return this._cold_blooded
    }
    set cold_blooded(value) {
        this._cold_blooded = value
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false