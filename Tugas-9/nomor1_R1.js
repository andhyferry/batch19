class Ape {
    constructor(name) {
        this._name = name;
    }
    yell() {
        return console.log('teriakan si ' +this._name+ ' adalah AUOO')
    }
}

class Frog {
    constructor(name) {
        this._name = name;
    }
    jump() {
        return console.log('lompatan si ' +this._name+ ' berbunyi HOP HOP!');
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 