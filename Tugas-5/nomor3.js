function sum(startNum, finishNum, step) {
    var angka = []
    var jumlah = 0
    if (startNum != null && finishNum != null && step != null) {
        if (startNum >= finishNum) {
            for (startNum; startNum >= finishNum; startNum -= step) {
                angka.push(startNum)
            }
            for (var i = 0; i< angka.length; i++) {
                jumlah += angka[i]
            }
            return jumlah
    
        } else if (startNum <= finishNum) {
            for (startNum; startNum <= finishNum; startNum += step) {
               angka.push(startNum)
            }
            for (var j = 0; j< angka.length; j++) {
                jumlah += angka[j]
            }
    
            return jumlah
        } 
    } else if (startNum != null && finishNum != null && step == null) {
        if (startNum >= finishNum) {
            for (startNum; startNum >= finishNum; startNum -= 1) {
                angka.push(startNum)
            }
            for (var i = 0; i< angka.length; i++) {
                jumlah += angka[i]
            }
            return jumlah
    
        } else if (startNum <= finishNum) {
            for (startNum; startNum <= finishNum; startNum += 1) {
               angka.push(startNum)
            }
            for (var j = 0; j< angka.length; j++) {
                jumlah += angka[j]
            }
    
            return jumlah
        } 
    } else if (startNum != null && finishNum == null && step == null) {
        return startNum
    }
     else {
         return jumlah
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 