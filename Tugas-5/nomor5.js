function balikKata (kata) {
    var word = kata
    var newWord = ''

    for (var i = word.length-1; i >= 0; i--) {
        newWord = newWord + word[i]
    }

    return newWord
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 