var people = [ ["Bruce", "Banner", "male", 1975], 
             ["Natasha", "Romanoff", "female"] ]

var contoh = [["Abduh", "Muhamad", "male", 1992], 
              ["Ahmad", "Taufik", "male", 1985]]

var people2 = [ ["Tony", "Stark", "male", 1980], 
                ["Pepper", "Pots", "female", 2023] ]

function arrayToObject(arr) {
    var masukan = arr.length
    var objMasukan = {}
    var now = new Date()
    var thisYear = now.getFullYear()
    for (var i = 0; i < masukan; i++) {
        if (arr[i][0] != null) {
            objMasukan['firstname'] = arr[i][0]
        } else {
            objMasukan['firstname'] = 'Invalid Firstname'
        }
        if (arr[i][1] != null) {
            objMasukan['lastname'] = arr[i][1]
        } else {
            objMasukan['lastname'] = 'Invalid Lastname'
        }
        if (arr[i][2] != null) {
            objMasukan['gender'] = arr[i][2]
        } else {
            objMasukan['gender'] = 'Invalid Gender'
        }
        if (arr[i][3] != null && thisYear > arr[i][3]) {
            var dapatTll = arr[i][3]
        var umur = thisYear - dapatTll
        objMasukan['age'] = umur
        } else {
            objMasukan['age'] = 'Invalid Birth Year'
        }

        console.log(i+1 + '. ' + arr[i][0] + ' ' + arr[i][1]);
        console.log(objMasukan);
        console.log('\n');
    }
}

arrayToObject(people) 

console.log('================================')
console.log('\n')
arrayToObject(people2)