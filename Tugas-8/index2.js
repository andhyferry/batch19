var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
function baca(time, books, jumlah) {
    if (jumlah < books.length) {
        readBooksPromise(time,books[jumlah])
        .then(function (sisaWaktu) {
            if (sisaWaktu > 0) {
                jumlah += 1
                baca(sisaWaktu, books, jumlah)
            }
        })
        .catch(function () {
            return books
        })
    }
    
}

baca(10000, books, 0)