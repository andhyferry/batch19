// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function membaca(time, books, jumlah) {
    if (jumlah < books.length) {
        readBooks (time, books[jumlah], function(sisaWaktu) {
            if (sisaWaktu > 0) {
                jumlah += 1
                membaca(sisaWaktu, books, jumlah)
            }
        })
    }
}

membaca(10000, books,0)