//if-else

var nama = 'Jane';
var peran = 'Penyihir';

if (nama == 'Jane' && peran == 'Penyihir') { //value pada kondisi ini harus sesuai dengan input yang diberikan
    console.log('Halo ' + nama + ', Pilih peranmu untuk bermain game!')
    if (nama == 'Jane' && peran == 'Penyihir') {
        console.log("Selamat datang di Dunia Werewolf, " +nama);
        console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
    } else if (nama == 'Jenita' && peran == 'Guard') {
        console.log("Selamat datang di Dunia Werewolf, " +nama);
        console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if (nama == 'Junaedi' && peran == 'Werewolf') {
        console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!")
    }
} else {
    console.log('Nama harus diisi!')
}


//switch

var tanggal = 31;
var valueT; 
var bulan = 12;
var valueB; 
var tahun = 1996;
var valueTn;


switch(tanggal) {
    case 1: { valueT = 1; break; }
    case 2: { valueT = 2; break; }
    case 3: { valueT = 3; break; }
    case 4: { valueT = 4; break; }
    case 5: { valueT = 5; break; }
    case 6: { valueT = 6; break; }
    case 7: { valueT = 7; break; }
    case 8: { valueT = 8; break; }
    case 9: { valueT = 9; break; }
    case 10: { valueT = 10; break; }
    case 11: { valueT = 11; break; }
    case 12: { valueT = 12; break; }
    case 13: { valueT = 13; break; }
    case 14: { valueT = 14; break; }
    case 15: { valueT = 15; break; }
    case 16: { valueT = 16; break; }
    case 17: { valueT = 17; break; }
    case 18: { valueT = 18; break; }
    case 19: { valueT = 19; break; }
    case 20: { valueT = 20; break; }
    case 21: { valueT = 21; break; }
    case 22: { valueT = 22; break; }
    case 23: { valueT = 23; break; }
    case 24: { valueT = 24; break; }
    case 25: { valueT = 25; break; }
    case 26: { valueT = 26; break; }
    case 27: { valueT = 27; break; }
    case 28: { valueT = 28; break; }
    case 29: { valueT = 29; break; }
    case 30: { valueT = 30; break; }
    case 31: { valueT = 31; break; }
    default:  { console.log('Berikan angka yang sesuai dengan perhitungan masehi'); }
}

switch(bulan) {
    case 1: { valueB = 'Januari'; break; }
    case 2: { valueB = 'Februari'; break; }
    case 3: { valueB = 'Maret'; break; }
    case 4: { valueB = 'April'; break; }
    case 5: { valueB = 'Mei'; break; }
    case 6: { valueB = 'Juni'; break; }
    case 7: { valueB = 'Juli'; break; }
    case 8: { valueB = 'Agustus'; break; }
    case 9: { valueB = 'September'; break; }
    case 10: { valueB = 'Oktober'; break; }
    case 11: { valueB = 'November'; break; }
    case 12: { valueB = 'Desember'; break; }
    default:  { console.log('Gunakan bulan yang sesuai dengan jumlah di kalender masehi'); }
}

switch(tahun) {
    case 1990: { valueTn = '1990'; break; }
    case 1991: { valueTn = '1991'; break; }
    case 1992: { valueTn = '1992'; break; }
    case 1993: { valueTn = '1993'; break; }
    case 1994: { valueTn = '1994'; break; }
    case 1995: { valueTn = '1995'; break; }
    case 1996: { valueTn = '1996'; break; }
    case 1997: { valueTn = '1997'; break; }
    case 1998: { valueTn = '1998'; break; }
    case 1999: { valueTn = '1999'; break; }
    case 2000: { valueTn = '2000'; break; }
    case 2001: { valueTn = '2001'; break; }
    case 2002: { valueTn = '2002'; break; }
    case 2003: { valueTn = '2003'; break; }
    case 2004: { valueTn = '2004'; break; }
    case 2005: { valueTn = '2005'; break; }
    case 2006: { valueTn = '2006'; break; }
    case 2007: { valueTn = '2007'; break; }
    case 2008: { valueTn = '2008'; break; }
    case 2009: { valueTn = '2009'; break; }
    case 2010: { valueTn = '2010'; break; }
    case 2011: { valueTn = '2011'; break; }
    case 2012: { valueTn = '2012'; break; }
    case 2013: { valueTn = '2013'; break; }
    case 2014: { valueTn = '2014'; break; }
    case 2015: { valueTn = '2015'; break; }
    case 2016: { valueTn = '2016'; break; }
    case 2017: { valueTn = '2017'; break; }
    case 2018: { valueTn = '2018'; break; }
    case 2019: { valueTn = '2019'; break; }
    case 2020: { valueTn = '2020'; break; }
    default:  { console.log('Mohon maaf saat ini sistem hanya mendukung input tahun 1990 sampai 2020'); }  
    
}

console.log(valueT + " " + valueB + " " + valueTn);