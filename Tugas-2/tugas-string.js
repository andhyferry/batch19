//jawaban soal nomor 1

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
var spasi = ' ';

//cara satu, menggunakan metode concat, agak ribet, karena harus menulis var spasi 
//satu-satu setiap kata
var kalimatbaru = word.concat(spasi,second,spasi,third, spasi, fourth, spasi, 
    fifth, spasi, sixth, spasi,seventh);

//cara dua, menggunakan metode join dengan memasukan tiap kata dalam suatu array
//metode join memiliki parameter untuk memisahkan kata-kata pada array, dalam hal ini adalah spasi
var goodsentence = [word, second,third,fourth, fifth, sixth,seventh].join(' ');

console.log(kalimatbaru);
console.log(goodsentence);

//jawaban soal nomor 2

var sentence = "I am going to be React Native Developer"; 
var pisah = sentence.split(" "); //memisahkan tiap kata menjadi satu array saja

var FirstWord = pisah[0];
var secondWord = pisah[1];
var thirdWord = pisah[2]; 
var fourthWord = pisah[3];  
var fifthWord = pisah[4];  
var sixthWord = pisah[5]; 
var seventhWord = pisah[6];  
var eighthWord = pisah[7]; 


console.log('First Word: ' + FirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

//jawaban soal nomor 3

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); //mengurai kalimat dengan substring
var secondWord2 = sentence2.substring(4, 14); 
var thirdWord2 = sentence2.substring(15, 17); 
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25); 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);